<?php
    require_once '../vendor/autoload.php';
    use \conf\Connect as Connexion;
    use racoin\backend\controller\AnnonceController as AnnonceController;
    use racoin\backend\controller\AdmController as AdmController;

    Connexion::startEloquent("../src/conf/configb.ini");

    $app = new \Slim\Slim(array('view' => new \Slim\Views\Twig(),
                                'templates.path'=>'../src/racoin/backend/view'));

    //$app->view->setTemplatesDirectory('../src/racoin/backend/view');

    $view = $app->view();
    $view->parserOptions = ['debug'=>true];
    $view->parserExtensions = [
        new \Slim\Views\TwigExtension()
    ];

    $app->get('/', function(){
        $admin = new AdmController();
        $admin->login();
    })->name('login');

    $app->post('/', function(){
        $admin = new AdmController();
        $admin->login();
    });

    $app->get('/dashboard', function() use($app){
        //echo $app->render('Dashboard.html.twig');
        $annonce = new AnnonceController();
        $annonce->dashboard();
    })->name('dashboard');

    $app->get('/annonces', function() use($app){
        $req = $app->request();
        $page = $req->get('page');
        $annonce = new AnnonceController();
        $annonce->annonces($page);
    })->name('annonces');

    $app->get('/annonce/', function() use($app){
        $req = $app->request();
        $nom = $req->get('nom');
        $annonce = new AnnonceController();
        $annonce->annonce($nom);
    })->name('annonce');

    $app->post('/annonce/', function() use($app){
        $req = $app->request();
        $nom = $req->get('nom');
        $status = new AnnonceController();
        $status->statusAnnonce($nom);
    });

    $app->get('/authentication', function(){
        $auth = new AnnonceController();
        $auth->authentication();
    })->name('authentication');

    $app->get('/logout', function(){
        $logout = new AdmController();
        $logout->logout();
    })->name('logout');

    $app->run();
?>
