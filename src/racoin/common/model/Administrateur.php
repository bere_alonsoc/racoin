<?php

namespace racoin\common\model;
use Illuminate\Database\Eloquent\Model as Model;

class Administrateur extends Model {
    protected $table = "administrateur";
    protected $id = "id";
    public $timestamps=false;

}
