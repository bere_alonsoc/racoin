<?php

namespace racoin\common\model;
use Illuminate\Database\Eloquent\Model as Model;

class Categorie extends Model {
    protected $table = "categorie";
    protected $id = "id";
    public $timestamps=false;

    public function annonces(){
      return $this->hasMany('\racoin\common\model\Annonce', 'cat_id');
    }
}
