<?php
/**
 * Created by PhpStorm.
 * User: nayeli
 * Date: 8/12/15
 * Time: 09:04 PM
 */

namespace racoin\common\model;
use Illuminate\Database\Eloquent\Model as Model;

class Departement extends Model {
    protected $table = "departement";
    protected $id = "id";
    public $timestamps=false;

    public function annonces() {
        return $this->hasMany('\racoin\common\model\Annonce', 'dep_id');
    }


}
