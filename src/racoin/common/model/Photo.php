<?php
/**
 * Created by PhpStorm.
 * User: nayeli
 * Date: 8/12/15
 * Time: 08:40 PM
 */

namespace src\racoin\common\model;
use Illuminate\Database\Eloquent\Model as Model;

class Photo extends Model {
    protected $table = "photo";
    protected $id = "id";
    public $timestamps=false;

    public function annonces(){
      return $this->belongsToMany('\racoin\common\model\Departement','id_annonce');
    }
}
