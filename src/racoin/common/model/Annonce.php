<?php

namespace racoin\common\model;
use Illuminate\Database\Eloquent\Model as Model;

class Annonce extends Model {
    protected $table = "annonce";
    protected $id = "id";
    public $timestamps=false;

    public function departement(){
      return $this->belongsTo('\racoin\common\model\Departement','dep_id');
    }

    public function categorie(){
      return $this->belongsTo('\racoin\common\model\Departement','dep_id');
    }

    public function photos(){
      return $this->belongsToMany('\racoin\common\model\Departement','id_photo');
    }


}
