<?php

namespace racoin\common\model;
use Illuminate\Database\Eloquent\Model as Model;

class APIKey extends Model {
    protected $table = "apikey";
    protected $id = "id";
    public $timestamps=false;

}
