<?php
    namespace racoin\api\controller;
    use racoin\common\model\Annonce as Annonce;

    class AnnonceurController{

        public function getAnnonceur($email){
            $app = \Slim\Slim::getInstance();
            $a= Annonce::all()->where('mail_a',$email)->first();

            if($a){
                $annonceur = array(
                    "Annonceur"=>array(
                        "nom"=>$a->nom_a,
                        "prenom"=>$a->prenom_a,
                        "mail"=>$a->mail_a,
                        "tel"=>$a->tel_a,
                        "ville"=>$a->ville),
                    "Links"=>[
                        'annonces' => ['href'=>$app->urlFor('annu2ann',['email'=>$email])]
                    ]
                );
                $app->response->headers->set('Content-Type','application/json');
                echo json_encode($annonceur);
            }else{
                $annonceur = array(
                    "Error"=>"Resource ".$email." not found"
                );
                $app->response->setStatus(404);
                $app->response->headers->set('Content-Type','application/json');
                echo json_encode($annonceur);
            }
        }

        public function getAnnonces($email){
            $app = \Slim\Slim::getInstance();
            $an = Annonce::all()->where('mail_a',$email);

            if($an){
              foreach($an as $a){
                $annonce = array(
                    "Annonce"=>array(
                        "id"=>$a->id,
                        "titre"=>$a->titre,
                        "descriptif"=>$a->descriptif,
                        "prix"=>$a->prix,
                        "ville"=>$a->ville,
                        "code postal"=>$a->code_postal,
                        "date de mise en ligne"=>$a->date_online),
                    "Links"=>[
                        'categorie' => ['href'=>$app->urlFor('ann2cat',['id'=>$a->id])],
                        'annonceur' => ['href'=>$app->urlFor('annonceur',['email'=>$a->mail_a])],
                        'departement' => ['href'=>$app->urlFor('departement',['id'=>$a->dep_id])],
                        //'photo' => ['href'=>$app->urlFor('photo',['id'=>$a->])]
                    ]
                  );
                $res[] = $annonce;
              }
              $app->response->headers->set('Content-Type','application/json');
              echo json_encode($res);
            }else{
                $annonce = array(
                    "Error"=>"Resource ".$id." not found"
                );
                $app->response->setStatus(404);
                $app->response->headers->set('Content-Type','application/json');
                echo json_encode($annonce);
            }
        }

        public function putCordonnees($id_annonce, $data) {
            $app = \Slim\Slim::getInstance();
            $annonce = Annonce::find($id_annonce);
            $mod = array();
            if($annonce) {
                foreach($data as $key => $value) {
                    if($key == 'nom_annonceur' || $key == 'prenom_annonceur' || $key == 'mail_annonceur' || $key == 'telephone_annonceur') {

                        $col = explode("_", $key);
                        $col2 = $col[0].'_a';
                        $annonce->$col2 = $value;
                        $mod[$col[0]] = $value;
                    }
                }

                if($annonce->save() && sizeof($mod) > 0) {
                    $message = array(
                        "Message" => "Resource ".$id_annonce." modified",
                        "Modified" => $mod
                    );
                    $app->response->headers->set('Content-Type','application/json');
                    echo json_encode($message);
                } else {
                    $message = array(
                        "Error"=>"Resource ".$id_annonce." not modified, invalid data",
                    );
                    $app->response->setStatus(404);
                    $app->response->headers->set('Content-Type','application/json');
                    echo json_encode($message);
                }
            } else {
                $message = array(
                    "Error"=>"Resource ".$id_annonce." not found",
                );
                $app->response->setStatus(404);
                $app->response->headers->set('Content-Type','application/json');
                echo json_encode($message);
            }

        }
    }

?>
