<?php

namespace racoin\api\controller;
use racoin\common\model\Categorie as Categorie;

class CategorieController {

  public function getCategorie($id){
      $app = \Slim\Slim::getInstance();
      //$head = $app->response->headers->set('Content-Type', 'application/json');
      $c = Categorie::find($id);

      if($c){
          $categorie = array(
              "Categorie"=>$c->toArray(),
              "Links"=>[
                  'annonces'=>['href'=>$app->urlFor('categorie',['id'=>$c->id])]
                ]);
                $app->response->headers->set('Content-Type','application/json');
                echo json_encode($categorie);
      }else{
        $annonce = array(
            "Error"=>"Resource ".$id." not found"
        );
        $app->response->setStatus(404);
        $app->response->headers->set('Content-Type','application/json');
        echo json_encode($annonce);
      }

      /*
      $cat->type = 'Categorie';
      $res=[
        'categorie'=>$cat->toArray(),
        'links'=>[
          'annonces'=>['href'=> $this->app->urlFor('cat2ann',['id'=>$cat->id])]
      ]
    ];*/
  }

  public function collectionCategories(){
      $app = \Slim\Slim::getInstance();
      $co = Categorie::all();
      $res = array();

      foreach($co as $c){
        $col=array(
            "Categorie"=>$c->toArray(),
            "Links"=>[
                //'anonces'=>['href'=>$app->urlFor('annonces',['id'=>$c->id])]
            ]);
        $res[]=$col;
      }
      $app->response->headers->set('Content-Type','application/json');
      echo json_encode($res);
  }

  public function getAnnonces($id){
      $app = \Slim\Slim::getInstance();
      $an = Categorie::find($id)->annonces;
      $res = array();

      if($an){
        foreach($an as $a){
          $annonce = array(
              "Annonce"=>array(
                  "id"=>$a->id,
                  "titre"=>$a->titre,
                  "descriptif"=>$a->descriptif,
                  "prix"=>$a->prix,
                  "ville"=>$a->ville,
                  "code postal"=>$a->code_postal,
                  "date de mise en ligne"=>$a->date_online),
              "Links"=>[
                  'categorie' => ['href'=>$app->urlFor('ann2cat',['id'=>$a->id])],
                  'annonceur' => ['href'=>$app->urlFor('annonceur',['email'=>$a->mail_a])],
                  'departement' => ['href'=>$app->urlFor('departement',['id'=>$a->dep_id])],
                  //'photo' => ['href'=>$app->urlFor('photo',['id'=>$a->])]
              ]
            );
          $res[] = $annonce;
        }
        $app->response->headers->set('Content-Type','application/json');
        echo json_encode($res);
      }else{
          $annonce = array(
              "Error"=>"Resource ".$id." not found"
          );
          $app->response->setStatus(404);
          $app->response->headers->set('Content-Type','application/json');
          echo json_encode($annonce);
      }
  }

  public function findAnnonceByWord($id_cat, $word){
        $app = \Slim\Slim::getInstance();
        $res = array();

        try {
            // Search by title
            $annonces = Annonce::where('cat_id', $id_cat)->where('titre', 'LIKE', "%$word%")->get();

            if($annonces) {
                foreach($annonces as $annonce) {
                    $res[] = $annonce;
                }
            }

            $annonces = Annonce::where('cat_id', $id_cat)->where('descriptif', 'LIKE', "%$word%")->get();

            if($annonces) {
                foreach($annonces as $annonce) {
                    $res[] = $annonce;
                }
            }
        } catch(\Exception $ex) {
            $an = null;
        }

        if($res){
            foreach($res as $a){
                $annonce = array(
                    "Annonce"=>array(
                        "id"=>$a->id,
                        "titre"=>$a->titre,
                        "descriptif"=>$a->descriptif,
                        "prix"=>$a->prix,
                        "ville"=>$a->ville,
                        "code postal"=>$a->code_postal,
                        "date de mise en ligne"=>$a->date_online),
                    "Links"=>[
                        'categorie' => ['href'=>$app->urlFor('ann2cat',['id'=>$a->id])],
                        'annonceur' => ['href'=>$app->urlFor('annonceur',['email'=>$a->mail_a])],
                        'departement' => ['href'=>$app->urlFor('departement',['id'=>$a->dep_id])],
                        //'photo' => ['href'=>$app->urlFor('photo',['id'=>$a->])]
                    ]
                );
                $res[] = $annonce;
            }
            $app->response->headers->set('Content-Type','application/json');
            echo json_encode($res);
        }else{
            $annonce = array(
                "Error"=>"Resources not found"
            );
            $app->response->setStatus(404);
            $app->response->headers->set('Content-Type','application/json');
            echo json_encode($annonce);
        }
    }
}
?>
