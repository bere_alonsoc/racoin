<?php
    namespace racoin\api\controller;
    use racoin\common\model\Photo as Photo;

    class PhotoController{
        public function getPhoto($id){
            $app = \Slim\Slim::getInstance();
            $a = Photo::find($id);
            if($a){
                $photo = array(
                    "Photo"=>array(
                        "id"=>$a->id,
                        "src"=>$a->src,
                        "titre"=>$a->titre),
                    "Links"=>[
                        /*'categorie' => ['href'=>$app->urlFor('ann2cat',['id'=>$a->id])],
                        'annonceur' => ['href'=>$app->urlFor('annonceur',['email'=>$a->mail_a])]*/
                    ]
                );
                $app->response->headers->set('Content-Type','application/json');
                echo json_encode($photo);

            }else{
                $photo = array(
                    "Error"=>"Resource ".$id." not found"
                );
                $app->response->setStatus(404);
                $app->response->headers->set('Content-Type','application/json');
                echo json_encode($photo);
            }
        }
    }
?>
