<?php
    namespace racoin\api\controller;
    use racoin\common\model\Departement as Departement;

    class DepartementController{

      public function getDepartement($id){
          $app = \Slim\Slim::getInstance();
          $a = Departement::find($id);
          if($a){
              $departement = array(
                  "Departement"=>array(
                      "id"=>$a->id,
                      "numero"=>$a->numero,
                      "libelle"=>$a->libelle),
                  "Links"=>[
                      'annonce' => ['href'=>$app->urlFor('cat2ann',['id'=>$a->id])]
                  ]
              );
              $app->response->headers->set('Content-Type','application/json');
              echo json_encode($departement);

          }else{
              $departement = array(
                  "Error"=>"Resource ".$id." not found"
              );
              $app->response->setStatus(404);
              $app->response->headers->set('Content-Type','application/json');
              echo json_encode($departement);
          }
      }

      public function getAnnonces($id){
          $app = \Slim\Slim::getInstance();
          $an = Categorie::find($id)->annonces;

          if($an){
            foreach($an as $a){
              $annonce = array(
                  "Annonce"=>array(
                      "id"=>$a->id,
                      "titre"=>$a->titre,
                      "descriptif"=>$a->descriptif,
                      "prix"=>$a->prix,
                      "ville"=>$a->ville,
                      "code postal"=>$a->code_postal,
                      "date de mise en ligne"=>$a->date_online),
                  "Links"=>[
                      'categorie' => ['href'=>$app->urlFor('ann2cat',['id'=>$a->id])],
                      'annonceur' => ['href'=>$app->urlFor('annonceur',['email'=>$a->mail_a])],
                      'departement' => ['href'=>$app->urlFor('departement',['id'=>$a->dep_id])],
                      //'photo' => ['href'=>$app->urlFor('photo',['id'=>$a->])]
                  ]
                );
              $res[] = $annonce;
            }
            $app->response->headers->set('Content-Type','application/json');
            echo json_encode($res);
          }else{
              $annonce = array(
                  "Error"=>"Resource ".$id." not found"
              );
              $app->response->setStatus(404);
              $app->response->headers->set('Content-Type','application/json');
              echo json_encode($annonce);
          }
      }
    }
?>
