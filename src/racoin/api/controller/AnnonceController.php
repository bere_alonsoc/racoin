<?php
namespace racoin\api\controller;
use racoin\common\model\Annonce as Annonce;
use racoin\common\model\Categorie as Categorie;

class AnnonceController{
    public function getAnnonce($id){
      $app = \Slim\Slim::getInstance();
      $a = Annonce::find($id);
      if($a && $a->status==1){
          $annonce = array(
              "Annonce"=>array(
                  "id"=>$a->id,
                  "titre"=>$a->titre,
                  "descriptif"=>$a->descriptif,
                  "prix"=>$a->prix,
                  "ville"=>$a->ville,
                  "code postal"=>$a->code_postal,
                  "date de mise en ligne"=>$a->date_online),
              "Links"=>[
                  'categorie' => ['href'=>$app->urlFor('ann2cat',['id'=>$a->id])],
                  'annonceur' => ['href'=>$app->urlFor('annonceur',['email'=>$a->mail_a])],
                  'departement' => ['href'=>$app->urlFor('departement',['id'=>$a->dep_id])],
                  //'photo' => ['href'=>$app->urlFor('photo',['id'=>$a->])]
              ]
          );
          $app->response->headers->set('Content-Type','application/json');
          echo json_encode($annonce);

        }else{
          $annonce = array(
              "Error"=>"Resource ".$id." not found"
          );
          $app->response->setStatus(404);
          $app->response->headers->set('Content-Type','application/json');
          echo json_encode($annonce);
        }

    }

    public function collectionAnnonces(){
        $app = \Slim\Slim::getInstance();
        $x = Annonce::all();
        $res = array();

        if($x && sizeof($x) > 0){
            foreach($x as $a){
                if($a->status == 1) {
                    $annonce = array(
                        "Annonce"=>array(
                            "id"=>$a->id,
                            "titre"=>$a->titre,
                            "descriptif"=>$a->descriptif,
                            "prix"=>$a->prix,
                            "ville"=>$a->ville,
                            "code postal"=>$a->code_postal,
                            "date de mise en ligne"=>$a->date_online),
                        "Links"=>[
                            'categorie' => ['href'=>$app->urlFor('ann2cat',['id'=>$a->id])],
                            'annonceur' => ['href'=>$app->urlFor('annonceur',['email'=>$a->mail_a])],
                            'departement' => ['href'=>$app->urlFor('departement',['id'=>$a->dep_id])],
                            //'photo' => ['href'=>$app->urlFor('photo',['id'=>$a->])]
                        ]
                    );
                    $res[] = $annonce;
                }
            }

            if(sizeof($res) > 0) {
                $app->response->headers->set('Content-Type','application/json');
                echo json_encode($res);
            } else {
                $annonce = array(
                    "Error"=>"Resources not found"
                );
                $app->response->setStatus(404);
                $app->response->headers->set('Content-Type','application/json');
                echo json_encode($annonce);
            }

        }else{
          $annonce = array(
              "Error"=>"Resources not found"
          );
          $app->response->setStatus(404);
          $app->response->headers->set('Content-Type','application/json');
          echo json_encode($annonce);
        }
    }

    public function getCategorie($id){
        $app = \Slim\Slim::getInstance();
        $a = Annonce::find($id);
        $ca = Categorie::find($a->cat_id);

        if($a){
            $annonce = array(
                "Annonce"=>array(
                    "id"=>$a->id,
                    "titre"=>$a->titre,
                    "descriptif"=>$a->descriptif,
                    "prix"=>$a->prix,
                    "ville"=>$a->ville,
                    "code postal"=>$a->code_postal,
                    "date de mise en ligne"=>$a->date_online,
                    "Categorie"=>array(
                        "id"=>$ca->id,
                        "libelle"=>$ca->libelle,
                        "descriptif"=>$ca->descriptif
                    )),
                    "Links"=>[
                        'annonce' => ['href'=>$app->urlFor('cat2ann',['id'=>$ca->id])],
                    ]
              );
          $app->response->headers->set('Content-Type','application/json');
          echo json_encode($annonce);
        }else{
            $annonce = array(
                "Error"=>"Resource ".$id." not found"
            );
            $app->response->setStatus(404);
            $app->response->headers->set('Content-Type','application/json');
            echo json_encode($annonce);
        }
    }

    public function getAnnonceur($id){
        $app = \Slim\Slim::getInstance();
        $a= Annonce::find($id);

        if($a){
            $annonceur = array(
                "Annonceur"=>array(
                    "nom"=>$a->nom_a,
                    "prenom"=>$a->prenom_a,
                    "mail"=>$a->mail_a,
                    "tel"=>$a->tel_a,
                    "ville"=>$a->ville),
                "Links"=>[
                    'annonces' => ['href'=>$app->urlFor('annu2ann',['email'=>$a->mail_a])]
                ]
            );
            $app->response->headers->set('Content-Type','application/json');
            echo json_encode($annonceur);
        }else{
            $annonceur = array(
                "Error"=>"Resource ".$id." not found"
            );
            $app->response->setStatus(404);
            $app->response->headers->set('Content-Type','application/json');
            echo json_encode($annonceur);
        }
    }


    public function postAnnonce($a){
        $app = \Slim\Slim::getInstance();
        $annonce = new Annonce();
        $annonce->titre = $a['titre'];
        $annonce->descriptif = $a['descriptif'];
        $annonce->ville = $a['ville'];
        $annonce->code_postal = $a['code_postal'];
        $annonce->prix = $a['prix'];
        $annonce->nom_a = $a['nom_annonceur'];
        $annonce->prenom_a = $a['prenom_annonceur'];
        $annonce->mail_a = $a['mail_annonceur'];
        $annonce->tel_a = $a['telephone_annonceur'];
        $annonce->passwd = $a['password'];
        $annonce->cat_id = $a['categorie'];
        $annonce->dep_id = $a['departement'];
        if ($annonce->save()){
            $message = array(
                "Message"=>"Resource created",
                'Ressource' => ['href'=>$app->urlFor('annonce',['id'=>$annonce->id])],
            );
            $app->response->headers->set('Content-Type','application/json');
            $app->response->headers->set('Location','/annonces/'.$annonce->id);
            $app->response->setStatus(201);
            echo json_encode($message);
        }else{
            $message = array(
                "Error"=>"Resource not created"
            );
            $app->response->setStatus(403);
            $app->response->headers->set('Content-Type','application/json');
            echo json_encode($message);
        }

    }

    public function postCategorie2Annonce($a,$id){
        $app = \Slim\Slim::getInstance();
        $annonce = new Annonce();
        $annonce->titre = $a['titre'];
        $annonce->descriptif = $a['descriptif'];
        $annonce->ville = $a['ville'];
        $annonce->code_postal = $a['code_postal'];
        $annonce->prix = $a['prix'];
        $annonce->nom_a = $a['nom_annonceur'];
        $annonce->prenom_a = $a['prenom_annonceur'];
        $annonce->mail_a = $a['mail_annonceur'];
        $annonce->tel_a = $a['telephone_annonceur'];
        $annonce->passwd = $a['password'];
        $annonce->cat_id = $id;
        $annonce->dep_id = $a['departement'];

        if ($annonce->save()){
            $message = array(
                "Message"=>"Resource created",
                'Ressource' => ['href'=>$app->urlFor('annonce',['id'=>$annonce->id])],
            );
            $app->response->headers->set('Content-Type','application/json');
            $app->response->setStatus(201);
            echo json_encode($message);
        }else{
            $message = array(
                "Error"=>"Resource not created"
            );
            $app->response->setStatus(403);
            $app->response->headers->set('Content-Type','application/json');
            echo json_encode($message);
        }
    }

    public function deleteAnnonce($id) {
        $app = \Slim\Slim::getInstance();
        $annonce = Annonce::find($id);

        if($annonce) {
            $annonce->status = 0;

            if($annonce->save()) {
                $message = array(
                    "Message"=>"Resource ".$id." deleted",
                );
                $app->response->headers->set('Content-Type','application/json');
                echo json_encode($message);
            } else {
                $message = array(
                    "Message"=>"Resource ".$id." not deleted",
                );
                $app->response->setStatus(404);
                $app->response->headers->set('Content-Type','application/json');
                echo json_encode($message);
            }

        } else {
            $message = array(
                "Message"=>"Resource ".$id." not found",
            );
            $app->response->setStatus(404);
            $app->response->headers->set('Content-Type','application/json');
            echo json_encode($message);
        }
    }

    public function findByPrix($min,$max){
        $app = \Slim\Slim::getInstance();
        $an = null;

        if($min && $max) {
            $an = Annonce::whereBetween('prix', [$min, $max])->get();
        } elseif ($min && !$max) {
            $an = Annonce::where('prix', '>=', $min)->get();
        } elseif (!$min && $max) {
            $an = Annonce::where('prix', '<=', $max)->get();
        }

        $res = array();

        if($an && sizeof($an) > 0){
            foreach($an as $a){
                if($a->status == 1) {
                    $annonce = array(
                        "Annonce"=>array(
                            "id"=>$a->id,
                            "titre"=>$a->titre,
                            "descriptif"=>$a->descriptif,
                            "prix"=>$a->prix,
                            "ville"=>$a->ville,
                            "code postal"=>$a->code_postal,
                            "date de mise en ligne"=>$a->date_online),
                        "Links"=>[
                            'categorie' => ['href'=>$app->urlFor('ann2cat',['id'=>$a->id])],
                            'annonceur' => ['href'=>$app->urlFor('annonceur',['email'=>$a->mail_a])],
                            'departement' => ['href'=>$app->urlFor('departement',['id'=>$a->dep_id])],
                            //'photo' => ['href'=>$app->urlFor('photo',['id'=>$a->])]
                        ]
                    );
                    $res[] = $annonce;
                }

            }
            if(sizeof($res) > 0) {
                $app->response->headers->set('Content-Type','application/json');
                echo json_encode($res);
            } else {
                $annonce = array(
                    "Error"=>"Resources not found"
                );
                $app->response->setStatus(404);
                $app->response->headers->set('Content-Type','application/json');
                echo json_encode($annonce);
            }
        }else{
            $annonce = array(
                "Error"=>"Resources not found"
            );
            $app->response->setStatus(404);
            $app->response->headers->set('Content-Type','application/json');
            echo json_encode($annonce);
        }
    }

    public function updateStatus($id, $status) {
        $app = \Slim\Slim::getInstance();
        $annonce = Annonce::find($id);

        foreach ($status as $key=>$value){
            if($key == 'status'){
              if($annonce) {
                  if($value >= 0 && $value < 4) {

                      $annonce->status = $value;

                      if($annonce->save()) {
                          $message = array(
                              "Message"=>"Resource's status ".$id." modified"
                          );
                          $app->response->headers->set('Content-Type','application/json');
                          echo json_encode($message);
                          break;
                      } else {
                          $message = array(
                              "Message"=>"Resource ".$id." not modified",
                          );
                          $app->response->setStatus(404);
                          $app->response->headers->set('Content-Type','application/json');
                          echo json_encode($message);
                      }

                  } else {
                      $message = array(
                          "Message"=>"Invalid status",
                      );
                      $app->response->setStatus(404);
                      $app->response->headers->set('Content-Type','application/json');
                      echo json_encode($message);
                  }
              } else {
                  $message = array(
                      "Message"=>"Resource ".$id." not found",
                  );
                  $app->response->setStatus(404);
                  $app->response->headers->set('Content-Type','application/json');
                  echo json_encode($message);
              }
          }else{
              $message = array(
                  "Message"=>"Invalid data",
              );
              $app->response->setStatus(404);
              $app->response->headers->set('Content-Type','application/json');
              echo json_encode($message);
          }
        }

    }
}

?>
