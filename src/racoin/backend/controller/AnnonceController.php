<?php
    namespace racoin\backend\controller;
    use racoin\common\model\Annonce as Annonce;
    use racoin\common\model\Categorie as Categorie;
    use \conf\Connect as Connexion;

    class AnnonceController {

      public function dashboard(){
          $app = \Slim\Slim::getInstance();

          session_start();

          if(isset($_SESSION['admin'])){
              $categories = Categorie::all();
              $total = Annonce::all()->count();
              $crees = Annonce::all()->where('status',1)->count();
              $online = Annonce::all()->where('status',2)->count();
              $supprimees = Annonce::all()->where('status',3)->count();
              $app->render('Dashboard.html.twig',['title'=>'Racoin',
                                                  'l_dashboard'=>[  'href'=>$app->urlFor('dashboard'),
                                                                    'title' => 'Dashboard'],
                                                  'l_annonces'=>[ 'href'=>$app->urlFor('annonces'),
                                                                  'title'=>'Annonces'],
                                                  'l_authentication'=>[ 'href'=>$app->urlFor('authentication'),
                                                                        'title'=>'Authentication'],
                                                  'l_logout'=>['href'=>$app->urlFor('logout'),
                                                                'title'=>'Logout'],
                                                  'count'=>['crees'=>$crees,
                                                            'online'=>$online,
                                                            'supprimees'=>$supprimees,
                                                            'total'=>$total],
                                                  'nom_adm'=>'Admin',
                                                  'categorie'=>$categories]);
          }else{
             echo "No existe";
          }

      }

      public function annonces($page){
          $app = \Slim\Slim::getInstance();

          session_start();

          if(isset($_SESSION['admin'])){
              $annonces = Annonce::where('status',1)->skip(20*($page-1))->take(20)->get();
              echo $app->render('AnnoncesView.html.twig',
                                              [ 'title'=>'Annonces',
                                                'annonces'=>$annonces,
                                                'linkde'=>$app->urlFor('annonce'),
                                                'l_dashboard'=>[  'href'=>$app->urlFor('dashboard'),
                                                                  'title' => 'Dashboard'],
                                                'l_annonces'=>[ 'href'=>$app->urlFor('annonces'),
                                                                'title'=>'Annonces'],
                                                'l_authentication'=>[ 'href'=>$app->urlFor('authentication'),
                                                                      'title'=>'Authentication'],
                                                'l_logout'=>['href'=>$app->urlFor('logout'),
                                                              'title'=>'Logout'],
                                                'page_s'=>$page+1,
                                                'page_p'=>$page-1]);
          }else{
             echo "No entra";
          }

      }

      public function annonce($nom){
          $app = \Slim\Slim::getInstance();

          session_start();

          if(isset($_SESSION['admin'])){
              $annonce = Annonce::all()->where('titre',$nom);
              echo $app->render('AnnonceView.html.twig',
                                              [ 'annonce'=>$annonce,
                                                'linkde'=>$app->urlFor('annonce'),
                                                'l_dashboard'=>[  'href'=>$app->urlFor('dashboard'),
                                                                  'title' => 'Dashboard'],
                                                'l_annonces'=>[ 'href'=>$app->urlFor('annonces'),
                                                                'title'=>'Annonces'],
                                                'l_authentication'=>[ 'href'=>$app->urlFor('authentication'),
                                                                      'title'=>'Authentication'],
                                                'l_logout'=>['href'=>$app->urlFor('logout'),
                                                            'title'=>'Logout']]);
          }else{
            echo "No entra";
          }
      }

      public function authentication(){
          $app = \Slim\Slim::getInstance();
          echo $app->render('Authentication.html.twig',
                                          [ 'title'=>'Racoin',
                                            'l_dashboard'=>['href'=>$app->urlFor('dashboard'),
                                                            'title' => 'Dashboard'],
                                            'l_annonces'=>[ 'href'=>$app->urlFor('annonces'),
                                                            'title'=>'Annonces'],
                                            'l_authentication'=>[ 'href'=>$app->urlFor('authentication'),
                                                                  'title'=>'Authentication']]);
      }

      public function statusAnnonce($nom){
          $app = \Slim\Slim::getInstance();
          $annonce = Annonce::where('titre',$nom)->first();
          var_dump($annonce);
          $message = '';
          //$status = $app->request->post();

          if(isset($_POST['valider'])){
              $annonce->status = 2;
              $message = 'Annonce validé';
          }
          if(isset($_POST['refuser'])){
              $annonce->status = 3;
              $message = 'Annonce supprimé';
          }

          if($annonce->save()){
              header("Location:".$app->redirect($app->urlFor('annonces')));
          }

      }

}
