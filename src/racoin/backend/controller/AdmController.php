<?php
  namespace racoin\backend\controller;
  use racoin\common\model\Administrateur as Administrateur;
  use racoin\common\model\APIKey as APIKey;
  use \conf\Connect as Connexion;

  class AdmController{
      public function login(){
          $app = \Slim\Slim::getInstance();

          $post = $app->request->post();
          $enc = '';
          $message="";

          if(isset($_POST['nom_apik'])){
              $length = 15;
              $key = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"), 0, $length);

              $api = new APIKey();
              $api->name = $_POST['nom_apik'];
              $api->key = $key;

              if($api->save()){
                $enc = "API Key : ";
                $message = $key;
              }

          }else{
              session_start();

              if(!isset($_SESSION['admin'])){
                  if($post){
                      $admin = Administrateur::where("nom",$post['nom'])->first();

                      if($admin && password_verify($post['pass'],$admin->pass)){
                          $_SESSION['admin'] = $admin;
                          header('Location:'.$app->redirect('dashboard'));
                      }else{
                        //  echo "No entra";
                        //echo password_hash($post['pass'],PASSWORD_DEFAULT, array('cost'=>12));
                          $message = "Mot de passe incorrect";
                      }
                  }
              }else{
                header('Location:'.$app->redirect('dashboard'));
              }}
              $app->render('Login.html.twig', [ 'enc_api'=>$enc,
                                                'api_key'=>$message]);

      }

      public function logout(){
          $app = \Slim\Slim::getInstance();

          session_start();

          if(isset($_SESSION['admin'])){
              session_destroy();
              header("Location:".$app->redirect($app->urlFor('login')));
          }else{
              echo "No sesión";
          }
      }

      public function apiKey(){
          $app = \Slim\Slim::getInstance();
          $post = $app->request->post();
      }
  }
