<?php
/**
 * Created by PhpStorm.
 * User: nayeli
 * Date: 8/12/15
 * Time: 07:22 PM
 */

namespace conf;

use Illuminate\Database\Capsule\Manager as DB;

class Connect {

    public static function startEloquent($file) {

        $conf = parse_ini_file( $file) ;

        $capsule = new DB();

        $capsule->addConnection( $conf ) ;
        $capsule->setAsGlobal();
        $capsule->bootEloquent();}
}
