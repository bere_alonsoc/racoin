<?php
  require '../vendor/autoload.php';
  use racoin\api\controller\CategorieController as CategorieController;
  use racoin\api\controller\AnnonceController as AnnonceController;
  use racoin\api\controller\AnnonceurController as AnnonceurController;
  use racoin\api\controller\DepartementController as DepartementController;
  use racoin\api\controller\PhotoController as PhotoController;
  use racoin\common\model\APIKey as APIKey;
  use \conf\Connect as Connexion;

  $app = new \Slim\Slim();
  Connexion::startEloquent("/../src/conf/configb.ini");

  //Not found
  $app->notFound(function() use ($app) {
      $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(400);
      $message = ["Error " => "URI not found"];
      echo json_encode($message);
  });

  //Obtenir 1 catégorie
  $app->get('/categories/:id','checkAPIKey',  function($id){
      (new CategorieController())->getCategorie($id);
  })->name('categorie');

  //Collection de catégories
  $app->get('/categories', 'checkAPIKey', function(){
      (new CategorieController())->collectionCategories();
  })->name('collectionCat');

  //Obtenir 1 annonce
  $app->get('/annonces/:id','checkAPIKey', function($id){
      (new AnnonceController())->getAnnonce($id);
  })->name('annonce');

  //Collection d'annonces
  $app->get('/annonces','checkAPIKey', function(){
      (new AnnonceController())->collectionAnnonces();
  })->name('collectionAn');

  //Obtenir les annonces d'une catégorie
  $app->get('/categories/:id/annonces','checkAPIKey', function($id) use ($app){
      $q = $app->request()->get('q');
      if($q) {
          (new CategorieController())->findAnnonceByWord($id, $q);
      } else {
          (new CategorieController())->getAnnonces($id);
      }
  })->name('cat2ann');

  //Obtenir la catégorie d'une annonce
  $app->get('/annonces/:id/categories','checkAPIKey', function($id){
      (new AnnonceController())->getCategorie($id);
  })->name('ann2cat');

  //Ajouter une annonce si la catégorie existe
  $app->post('/annonces','checkAPIKey', function() use($app){
      $annonce = $app->request->post();
      (new AnnonceController())->postAnnonce($annonce);
  });

  //Ajouter une annonces dans une catégorie
  $app->post('/categories/:id/annonces','checkAPIKey', function($id) use($app){
      $annonce = $app->request->post();
      (new AnnonceController())->postCategorie2Annonce($annonce, $id);
  });

  //Conditions de prix dans les recherches d'annonce
  $app->get('/annonces/','checkAPIKey', function() use ($app){
      $min = $app->request->get('min');
      $max = $app->request->get('max');
      (new AnnonceController())->findByPrix($min, $max);
  });

  //Obtenir les cordonnées d'un annonceur
  $app->get('/annonceurs/:email','checkAPIKey', function($email){
      (new AnnonceurController())->getAnnonceur($email);
  })->name('annonceur');

  $app->get('/annonces/:id/annonceurs','checkAPIKey', function($id){
      (new AnnonceController())->getAnnonceur($id);
  })->name('ann2annu');

  //Ajouter les cordonnées d'un annonceur
  $app->put('/annonces/:id/annonceurs','checkAPIKey', function($id) use ($app) {
      $data = $app->request->get();
      (new AnnonceurController())->putCordonnees($id, $data);
  });

  //Gérer un état des annonces
  $app->put('/annonces/:id/','checkAPIKey', function($id) use ($app) {
      $status = $app->request->get();
      (new AnnonceController())->updateStatus($id, $status);
  });

  //Supprimer un annonce
  $app->delete('/annonces/:id','checkAPIKey', function($id){
      (new AnnonceController())->deleteAnnonce($id);
  });

  //Obtenir les annonces



  //SUPPLEMENTAIRE POUR LES LINKS

  //Obtenir un département
  $app->get('/departements/:id','checkAPIKey', function($id){
      (new DepartementController())->getDepartement($id);
  })->name('departement');

  //Obtenir une photo
  $app->get('/photos/:id','checkAPIKey', function($id){
      (new PhotoController())->getPhoto($id);
  })->name('photo');

  //Obtenir toutes les annonces d'un département
  $app->get('/departements/:id/annonces','checkAPIKey', function($id){
      (new DepartementController())->getAnnonces($id);
  })->name('dep2ann');

  //Obtenir toutes les annonces d'un annonceur
  $app->get('/annonceurs/:email/annonces','checkAPIKey', function($email){
      (new AnnonceurController())->getAnnonces($email);
  })->name('annu2ann');

  function checkAPIKey() {
      $app = \Slim\Slim::getInstance();
      $reg = null;
      $key = $app->request()->get('apikey');

      if($key) {
          $reg = APIKey::where('key', $key)->first();
      }

      if($reg) {
          $count = intval($reg->count);
          $reg->count = $count + 1;
          $reg->save();
          return true;
      } else {
          $message = array(
              "Error"=>"Unauthorized"
          );
          $app->response->setStatus(401);
          $app->response->headers->set('Content-Type','application/json');
          echo json_encode($message);
          $app->stop();
          return false;
      }
  }

  $app->run();
?>
